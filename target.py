from enum import Enum


class OutputTarget(Enum):
    """OutputTarget однозначно указывает на метод отправки сообщений.

    Необходим для уменьшения ошибок в коде из-за опечаток
    """
    TELEGRAM = "telegram"
