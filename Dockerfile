FROM python@sha256:f66a8f3c71a99cad687c7f5b2faaaa869ef842d29672abd2cbc25b8f8bf94436 as builder

ENV USER=appuser
ENV UID=10001

RUN groupadd -g ${UID} python && \
    useradd -l -r -u ${UID} -g python python && \
    mkdir /usr/app && \
    chown python:python /usr/app

WORKDIR /usr/app

RUN python -m venv /usr/app/venv
ENV PATH="/usr/app/venv/bin:$PATH"
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt


FROM python@sha256:f66a8f3c71a99cad687c7f5b2faaaa869ef842d29672abd2cbc25b8f8bf94436

WORKDIR /usr/app
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --chown=python:python --from=builder /usr/app/venv ./venv
COPY --chown=python:python . .

USER ${USER}
ENV PATH="/usr/app/venv/bin:$PATH"
ENTRYPOINT ["python", "app.py"]
