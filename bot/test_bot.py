from unittest import TestCase, main
from string import ascii_letters
from random import choice
from typing import List, Literal

from .telegram_bot import TelegramBot


class TestTelegramBot(TestCase):
    def test_message_constructing(self):
        token: str = "super secret"
        bot: TelegramBot = TelegramBot(token=token)
        required_fields: List[Literal] = [
            "chat_id",
            "text"
        ]
        for chat_id in range(10):
            payload: str = "".join([choice(ascii_letters) for _ in range(10)])
            result = bot.debug_message(payload, chat_id=chat_id)
            self.assertTrue(isinstance(result, dict))
            self.assertListEqual(list(result.keys()), required_fields)


if __name__ == "__main__":
    main()
