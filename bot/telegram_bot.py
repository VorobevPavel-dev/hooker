from typing import Dict, Tuple, Any, Self
from requests import post, Response
from .bot import Bot
from argparse import ArgumentParser


class TelegramBot(Bot):
    _BASE_URL: str = "https://api.telegram.org/bot{}"
    _CHECK_URL: str = "/".join([_BASE_URL, "getMe"])
    _SEND_URL: str = "/".join([_BASE_URL, "sendMessage"])

    def __init__(self, **kwargs) -> None:
        self._token = kwargs.get("token")

        self._headers: Dict[str, str] = {
            'Content-type': 'application/json'
        }

        self._params: Dict[str, str] = {
            'parse_mode': "HTML"
        }

    def __construct_message(self, message: str, **kwargs) -> Dict[str, str]:
        return {
            'chat_id': kwargs.get("chat_id"),
            'text': message
        }

    def debug_message(self, message: str, **kwargs) -> str | Dict[str, str]:
        return self.__construct_message(message, **kwargs)

    def check(self) -> Tuple[int, str]:
        response: Response = post(
            url=self._CHECK_URL.format(self._token),
        )
        return response.status_code, response.text

    def send(self, message: str, **kwargs) -> Tuple[int, str]:
        response: Response = post(
            url=self._SEND_URL.format(self._token),
            headers=self._headers,
            params=self._params,
            json=self.__construct_message(message, **kwargs)
        )
        return response.status_code, response.text

    @staticmethod
    def load(source: Dict[str, Any]) -> Self:
        """load позволяет создать сущность из JSON следующего формата:
        ```json
        {
            "token": "<token>"
        }
        ```

        Args:
            source (Dict[str, Any]): данные для обработки

        Returns:
            Self: сущность класса
        """
        return TelegramBot(token=source.get("token"))

    def __dict__(self) -> Dict[str, Any]:
        masked_token: str = "<masked>" if self._token else "<null>"
        return {
            'token': masked_token,
            'headers': self._headers,
            'parameters': self._params,
            'base_url': self._BASE_URL,
            'send_url': self._SEND_URL,
            'check_url': self._CHECK_URL
        }


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--token", "-t", type=str, help="Token to use")
    parser.add_argument("--chat-id", "-c", type=str, help="ChatID to send test message")
    arguments = parser.parse_args()

    bot: TelegramBot = TelegramBot(token=arguments.token)
    print(bot.send("Hello. This is a test message", chat_id=arguments.chat_id))
