from typing import Tuple, Dict, Any, Self
from json import dumps


class Bot:
    """Bot содержит информацию о подключении к какому-либо сервису
    для отправки сообщений, но не содержит конкретной точки отправки.

    Например, бот для Telegram содержит информацию о подключении к API,
    но не содержит chat_id для отправки данных.

    Интерфейс класса позволяет проверить подключение, получить данные,
    которые будут отправлены на сервер и отправить сообщение.

    Также доступен статический метод `load` для формирования сущности
    на основе JSON, формат которого описан для каждого наследника в
    отдельности
    """

    def __init__(self, **kwargs) -> None:
        pass

    def check(self) -> Tuple[int, str]:
        """check подзволяет проверить корректность параметров бота
        путём запроса к API

        Returns:
            Tuple[int, str]: код ответа и сообщение
        """
        pass

    def debug_message(self, message: str, **kwargs) -> str | Dict[str, Any]:
        """debug_message позволяет получить те данные,
        что будут отправлены на сервер.

        Returns:
            str | Dict[str, Any]: данные для отправки
        """
        pass

    def __construct_message(self, message: str, **kwargs) -> str | Dict[str, str]:
        """__construct_message подготавливливает сырые данные
        для оправки на сервер.

        Проще говоря, преобразует данные в тот формат,
        который бует принят API

        Returns:
            str | Dict[str, str]: данные для отправки
        """
        pass

    def send(self, message: str, **kwargs) -> Tuple[int, str]:
        """send выполняет отправку данных на сервер"""
        pass

    @staticmethod
    def load(source: Dict[str, Any]) -> Self:
        pass

    def __str__(self) -> str:
        return dumps(self.__dict__())
