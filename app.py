from json import JSONDecodeError, dumps, loads
from toml import load
from argparse import ArgumentParser, BooleanOptionalAction
from typing import Callable, Dict, Any, List
from application_group import ApplicationGroup
from flask import Flask, request

from bot.bot import Bot
from bot.telegram_bot import TelegramBot
from handler.awx_handler import AWXHandler
from handler.gitlab_handler import GitlabHandler
from handler.handler import Handler
from target import OutputTarget

VERSION: str = "v0.1.1"

HANDLERS: Dict[str, Handler] = {
    "gitlab": GitlabHandler(),
    "awx": AWXHandler()
}

app = Flask(__name__)


@app.route("/webhook/<string:application>", methods=['POST'])
def gitlab_handler(application: str):
    # Проверка того, что параметр groups был передан
    provided_groups: str | None = request.args.get("groups")
    if not provided_groups:
        return {
            'status': 'request_error',
            'message': 'Parameter groups was not provided. Check address of hook'
        }, 400
    provided_groups: List[str] = str(provided_groups).split(',')

    # Проверка того, что все группы объявлены в конфигурационном файле
    for group in provided_groups:
        if group not in [str(v) for v in groups.keys()]:
            return {
                'status': 'request_error',
                'message': f'Group {group} is not initialized in config file'
            }, 400

    # Составление списка групп для уведомлений
    to_notify: List[ApplicationGroup] = [groups[group] for group in groups if group in provided_groups]

    # Формирование ответа на веб-хук
    try:
        data = loads(request.data)
        message, code = HANDLERS[application].handle(data)
        if code != 0:
            return {
                'status': 'handle_error',
                'message': 'Error during handling webhook',
                'handle_error_message': message,
                'internal_error_code': code
            }, 400
        # Уведомление групп
        for group in to_notify:
            group.notify(message)
        return {
            'status': 'success'
        }, 200
    except JSONDecodeError:
        return {
            'status': 'request_error',
            'message': 'Provided data is not a valid JSON'
        }, 500
    except Exception as err:
        return {
            'status': 'internal_error',
            'error': str(err)
        }, 500


if __name__ == "__main__":
    # Обработать параметры командной строки
    parser = ArgumentParser()
    parser.add_argument("--config", "-c", type=str, default="config.toml", help="Path to config file")
    parser.add_argument("--version", type=bool, action=BooleanOptionalAction)
    arguments = parser.parse_args()

    # Вывод версии, если необходимо
    if arguments.version:
        print(VERSION)
        exit(0)

    # Загрузка конфига в приложение

    config: Dict[str, Any] = dict()
    with open(arguments.config, 'r') as config_file:
        config = load(config_file)

    # Получение данных об объявленных ботах

    # Соответствие между целью отправки и соответствущм классом бота
    LOADERS: Dict[OutputTarget, Callable] = {
        OutputTarget.TELEGRAM: TelegramBot
    }
    # Соответствие между целью отправки и конкретной сущностью класса бота
    bots: Dict[OutputTarget, Bot] = dict()

    bot_config: Dict[str, Dict[str, str]] = config.get("targets")
    for target in OutputTarget:
        bots[target] = LOADERS[target].load(bot_config.get(target.value))

    # Получение данных о группах

    groups_config: Dict[str, Dict[str, str]] = config.get("outputs")
    # groups: List[ApplicationGroup] = [ApplicationGroup.load(c) for c in groups_config]
    groups: Dict[str, ApplicationGroup] = dict()
    for raw_group in groups_config:
        parsed_group: ApplicationGroup = ApplicationGroup.load(raw_group)
        groups[parsed_group.name] = parsed_group

    # Назначение группе бота
    for group in groups.values():
        group.sender = bots[group.target]

    app.run('0.0.0.0', 5000)
