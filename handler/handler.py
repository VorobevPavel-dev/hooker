from os.path import basename, realpath
from typing import Dict, Any
from jinja2 import FileSystemLoader, Environment


class Handler:
    """Handler - класс, позволяющий обработать хуки от различных приложений.
    """
    def __init__(self):
        # _base_dir - директория, где располагается даный скрипт
        #   необходимо для упрощения поиска и хранения шаблонов
        self._base_dir = realpath(__file__).removesuffix(basename(__file__))
        self._loader: FileSystemLoader = FileSystemLoader(f"{self._base_dir}/templates")
        self._env: Environment = Environment(loader=self._loader)
        self._env.trim_blocks = True
        self._env.lstrip_blocks = True

    def handle(self, data: Dict[str, Any]) -> str:
        pass
