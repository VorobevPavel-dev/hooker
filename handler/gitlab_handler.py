from typing import Dict, Any, Tuple, Callable, Self
from jinja2 import Template
from argparse import ArgumentParser
from enum import Enum
from inspect import getmembers, ismethod

from .handler import Handler


class Event(Enum):
    """Event является перечислением всех обрабатываемых типов хуков отночительно Gitlab
    """
    PUSH = "push"
    PIPELINE = "pipeline"


class PipelineStatus(Enum):
    """PipelineStatus является перечислением всех значений для поля
    `веб-хук["object_attributes"].get("status")`, которые подходят для
    формаирования уведомления.
    """
    SUCCESS = "success"
    RUNNING = "running"
    FAILED = "failed"
    CANCELLED = "canceled"


class GitlabHandler(Handler):
    def __init__(self) -> None:
        """Создание хэндлера для обработки хуков от Gitlab

        Создаёт необходимое для взаимодействия поле _meta, содержащее
        соответствие между типом события и его шаблоном и обработчиком.
        Для создания использует автогенерацию на предположении, что файлы для обработки
        лежат в директории (относительно задаваемого окружения в `Handler`) `/gitlab/` и
        имеют имена вида `{Event.value}.j2`, а фукции-обработчики - `__handle_{Event.value}`

        """
        super().__init__()
        self._NAME = type(self).__name__

        # Получение соттветствия вида <имя события>:<функция> метоов класса с префиксом __handle_
        __HANDLE_METHODS = \
            {k[0].removeprefix(f"_{self._NAME}__handle_"): k[1] for k in getmembers(self, predicate=ismethod)
                if k[0].removeprefix(f"_{self._NAME}__handle_") in [e.value for e in Event]}

        self._meta: Dict[Event, Tuple[Template, Callable[[Self, Template, Dict[str, Any]], Tuple[str, int]]]] = dict()
        """
        Словарь, содержащий информацию для каждого типа:
        Событие: (Шаблон для обработки, Функция-обработчик)

        Пример значения:

        ```python
        {
            <Event.PUSH: 'push'>: (<Template '/gitlab/push.j2'>, <bound method GitlabHandler.__handle_push of <__main__.GitlabHandler object at 0x7f7daba1c990>>),
            <Event.PIPELINE: 'pipeline'>: (<Template '/gitlab/pipeline.j2'>, <bound method GitlabHandler.__handle_pipeline of <__main__.GitlabHandler object at 0x7f7daba1c990>>)}
        }
        ```

        Пример получения данных:
        ```python
        template, handler = self._meta[Event.<EVENT_NAME>]
        ```
        """
        self._meta = \
            {event: (self._env.get_template(f"/gitlab/{event.value}.j2"), __HANDLE_METHODS[event.value])
                for event in Event}

    def __handle_push(self, template: Template, data: Dict[str, Any]) -> Tuple[str, int]:
        """Обработчик сообщений по типу PUSH

        Args:
            template (Template): шаблон для заполнения
            data (Dict[str, Any]): данные веб-хука

        Returns:
            Tuple[str, int]: заполненный шаблон, код ошибки
        """
        return template.render(data=data), 0

    def __handle_pipeline(self, template: Template, data: Dict[str, Any]) -> Tuple[str, int]:
        """Обработчик сообщений по типу PIPELINE

        Args:
            template (Template): шаблон для заполнения
            data (Dict[str, Any]): данные веб-хука

        Returns:
            Tuple[str, int]: заполненный шаблон, код ошибки
        """

        # Cписок статусов пайплайна, о котором можно уведомлять
        # notified_statuses: List[str] = ["success", "failed", "running", "canceled"]
        try:
            current_status: str | None = data["object_attributes"].get("status")
            PipelineStatus(current_status)
        except ValueError:
            return f"Status {current_status} is not in list for notification", 2
        return template.render(data=data), 0

    def handle(self, data: Dict[str, Any]) -> Tuple[str, int]:
        """Обрабатывает данные веб-хука. Возвращает обработанное сообщение и код ошибки.

        Список кодов ошибок:
            - 0: Обработка успешно завершена
            - 1: Тип события в хуке не обрабатывается
            - 2: Хук не подходит для обработки

        Args:
            data (Dict[str, Any]): словарь с данными веб-хука

        Returns:
            Tuple[str, int]: Обработанное сообщение, код ошибки
        """
        try:
            raw_event_type: str = data.get("object_kind")
            event: Event = Event(raw_event_type)
        except ValueError:
            return f"Invalid event type: {raw_event_type}. Supported: {','.join([event.value for event in Event])}", 1
        # Обработка KeyError не нужна из-за автогенерации данных
        template, handler = self._meta[event]
        return handler(template, data)

    @property
    def meta(self) -> Dict[Event, Tuple[Template, Callable]]:
        return self._meta

    @property
    def name(self) -> str:
        return self._NAME


if __name__ == "__main__":
    from json import load

    parser = ArgumentParser()
    parser.add_argument("-f", "--file", help="Input data")
    arguments = parser.parse_args()

    handler: GitlabHandler = GitlabHandler()
    with open(arguments.file, 'r') as f:
        print(handler.handle(load(f)))
