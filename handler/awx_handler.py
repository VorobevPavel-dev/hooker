from enum import Enum
from .handler import Handler
from typing import Dict, Any, Tuple
from jinja2 import Template


class TemplateStatus(Enum):
    SUCCESSFUL = "successful"
    RUNNING = "running"
    FAILED = "failed"


class AWXHandler(Handler):
    def __init__(self) -> None:
        super().__init__()
        self._name = type(self).__name__

        self._meta: Dict[TemplateStatus, Template] = dict()
        """Словарь соответствий для шаблонов

        Хранит информацию в виде <статус темплейта> - <шаблон для обработки>
        """
        self._meta = \
            {status: self._env.get_template(f"/awx/{status.value}.j2") for status in TemplateStatus}

    def handle(self, data: Dict[str, Any]) -> Tuple[str, int]:
        try:
            raw_status_type: str | None = data.get("status")
            status: TemplateStatus = TemplateStatus(raw_status_type)
        except ValueError:
            return f"Invalid status: {raw_status_type}. Supported: {','.join([status.value for status in TemplateStatus])}", 1
        template: Template = self._meta.get(status)
        return template.render(data=data), 0
