from inspect import getmembers, ismethod
from unittest import TestCase, main
from typing import List, Callable, Dict, Any
from os import listdir
from json import load
from os.path import realpath, basename

from .gitlab_handler import Event, GitlabHandler, PipelineStatus

EXAMPLE_DIRECTORY: str = realpath(__file__).removesuffix(basename(__file__)) + "/test"


class TestGitlabHandler(TestCase):
    """Тестирование класса GitlabHandler"""
    def test_fullness(self):
        """Проверка наличия функций вида __handle_ для всех событий класса Event
        """
        handler: GitlabHandler = GitlabHandler()
        handled_events: List[Callable] = \
            [k[0].removeprefix(f"_{handler.name}__handle_") for k in getmembers(handler, predicate=ismethod)
                if k[0].find("__handle_") != -1]
        self.assertListEqual(sorted(handled_events), sorted([event.value for event in Event]))

    def test_creation(self):
        """Проверка создания всех полей в __init__
        """
        handler: GitlabHandler = GitlabHandler()
        self.assertIsNotNone(handler.meta)
        self.assertIsInstance(handler.meta, dict)
        handlers_count: int = len(
            [k[0] for k in getmembers(handler, predicate=ismethod)
                if k[0].find("__handle_") != -1]
        )
        self.assertEqual(handlers_count, len(handler.meta))

    def test_push_handle(self):
        """Проверка генерации ответов на push хуки
        """
        handler: GitlabHandler = GitlabHandler()
        PREFIX: str = "push_"
        files: List[str] = ["/".join([EXAMPLE_DIRECTORY, f]) for f in listdir(EXAMPLE_DIRECTORY) if f.startswith(PREFIX)]
        for file in files:
            with open(file, 'r') as example:
                data: Dict[str, Any] = load(example)
                message, code = handler.handle(data)
                self.assertEqual(code, 0)
                self.assertTrue(len(message) > 0)

    def test_pipeline_handle(self):
        """Проверка генерации ответов на pipeline хуки

        Выполняется проверка хуков со следующими статусами:
            - pending
            - running
            - success
            - cancelled
        """
        handler: GitlabHandler = GitlabHandler()
        PREFIX: str = "pipeline_"
        files: List[str] = ["/".join([EXAMPLE_DIRECTORY, f]) for f in listdir(EXAMPLE_DIRECTORY) if f.startswith(PREFIX)]
        for file in files:
            with open(file, 'r') as example:
                data: Dict[str, Any] = load(example)
                status: str = data["object_attributes"]["status"]
                message, code = handler.handle(data)
                if status not in [s.value for s in PipelineStatus]:
                    self.assertEqual(code, 2, file)
                    self.assertRegex(message, r'^Status \S+ is not in list for notification$')
                else:
                    self.assertEqual(code, 0, file)
                    self.assertTrue(len(message) > 0)


if __name__ == "__main__":
    main()
