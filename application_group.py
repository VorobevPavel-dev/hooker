from typing import Tuple, Dict, Any, Self
from json import dumps

from bot.bot import Bot
from target import OutputTarget


class ApplicationGroup:
    def __init__(self, name: str, **settings) -> None:
        self._name: str = name
        self._settings = settings
        self._target: OutputTarget = OutputTarget(self._settings.get("output_target"))

        self._sender: Bot = None

    def notify(self, message: str) -> Tuple[int, str]:
        return self._sender.send(message, **self._settings)

    @staticmethod
    def load(data: Dict[str, Any]) -> Self:
        """load позволяет создать группу на основе данных в виде следующего JSON:
        ```json
        {
            "group_name": {
                "output_target": <target>,
                "setting1": "value1",
                "settingN": "valueN"
            }
        }
        ```

        В примере выше `target` может принимать только значения из `enum` класса
        `OutputTarget`, а `group_name` - произвольное строковое значение

        Args:
            data (Dict[str, Any]): данные для обработки

        Returns:
            Self: экземпляр класса ApplicationGroup
        """
        name: str = str(list(data.keys())[0])
        settings: Dict[str, Any] = data.get(name)
        return ApplicationGroup(name, **settings)

    @property
    def sender(self) -> Bot:
        return self._sender

    @property
    def target(self) -> OutputTarget:
        return self._target

    @property
    def name(self) -> str:
        return self._name

    @sender.setter
    def sender(self, sender: Bot) -> None:
        self._sender = sender

    def __dict__(self) -> Dict[str, Any]:
        return {
            'name': self._name,
            'target': self._target.value,
            'sender': self._sender.__dict__(),
            'settings': self._settings
        }

    def __str__(self) -> str:
        return dumps(self.__dict__())
